var generateCode = `
  var result = '<pre>function ' + pageName + '() {<br>';

  result += '<br>&#9;this.url = \\'' + url + '\\';<br>';

  for(index = 0; index < finalResultSet.length; index++) {
    if(!finalResultSet[index].elementGroup) {
      result += '<br>&#9;this.' + finalResultSet[index].name + ' = () => { return ' + (finalResultSet[index].parent == 'None' ? '' : 'this.' + finalResultSet[index].parent + '().') + '$(\\'' + finalResultSet[index].css + '\\'); }<br>';
    }
    else {
      result += '<br>&#9;this.' + finalResultSet[index].name + ' = () => { return ' + (finalResultSet[index].parent == 'None' ? 'element.' : 'this.' + finalResultSet[index].parent + '().') + 'all(by.css(\\'' + finalResultSet[index].css + '\\')); }<br>';
    }
  }

  result += '}<br><br>module.exports = new ' + pageName + '();<br></pre>'
`

function recordStop(e) {
  chrome.tabs.executeScript(null,
    { code: "var pageName = '-', variableNameRegex = new RegExp('^([a-z]|[A-Z]|$|_)+([a-z]|[A-Z]|[0-9]|$|_)*$'); while(!variableNameRegex.test(pageName)) pageName = window.prompt('Enter page name: (preferably in camelCase)\\n\\nPlease follow JavaScript variable naming standards and rules.'); window.document.location.reload(); url = window.prompt('Please enter the url:', window.location.href); finalResultSet = JSON.parse(window.document.getElementById('UhgRPHIvGBvcKQ').className);" + generateCode + "newWindow = window.open(null, null, 'width=450,height=550,location=no,menubar=no,status=no', true); newWindow.document.write(result);" });
  window.close();
}

function recordStart(e) {
  chrome.tabs.executeScript(null,
    { code: "startRecording(); " });
  window.close();
}

document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('record start').addEventListener('click', recordStart);
  document.getElementById('record stop').addEventListener('click', recordStop);
});